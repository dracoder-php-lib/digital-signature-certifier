<?php

namespace Dracoder\DigitalSignatureCertifier\Service;

class TextDigitalSignatureCertifier extends AbstractDigitalSignatureCertifier
{
    public function signFile(string $filename, string $output, array $headers = []): bool
    {
        return $this->pkcs7Sign($filename, $output, $headers);
    }
}
