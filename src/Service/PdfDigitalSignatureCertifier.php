<?php

namespace Dracoder\DigitalSignatureCertifier\Service;

use TCPDI;

class PdfDigitalSignatureCertifier extends AbstractDigitalSignatureCertifier
{
    /**
     * @param string $filename
     * @param string $output
     *
     * @return string
     */
    public function signFile(string $filename, string $output): string
    {
        $pdfData = file_get_contents($filename);

        return $this->signData($pdfData, $output);
    }

    /**
     * @param string $pdfData
     * @param string $output
     *
     * @return string
     */
    public function signData(string $pdfData, string $output): string
    {
        $pdf = new TCPDI(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pageCount = $pdf->setSourceData($pdfData);
        for ($i = 1; $i <= $pageCount; $i++) {
            $page = $pdf->importPage($i);
            $pdf->AddPage();
            $pdf->useTemplate($page);
        }

        $pdf->setSignature($this->getCertificateData(), $this->getPrivateKey(), $this->getPassword(), '', 1);

        return $pdf->Output($output, 'F');
    }
}
