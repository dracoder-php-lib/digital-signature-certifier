<?php

namespace Dracoder\DigitalSignatureCertifier\Service;

use RuntimeException;

abstract class AbstractDigitalSignatureCertifier
{
    /** @var resource $certificateData */
    private $certificateData;
    /** @var resource $privateKey */
    private $privateKey;
    private string $password;

    /**
     * AbstractDigitalSignatureCertifier constructor.
     *
     * @param string $pkcs12Certificate
     * @param string $password
     */
    public function __construct(string $pkcs12Certificate, string $password)
    {
        $this->retrieveCertificateData($pkcs12Certificate, $password);
    }

    /**
     * @return resource
     */
    protected function getCertificateData()
    {
        return $this->certificateData;
    }

    /**
     * @return resource
     */
    protected function getPrivateKey()
    {
        return $this->privateKey;
    }

    /**
     * @return string
     */
    protected function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $unsigned
     * @param string $out
     * @param array $headers
     *
     * @return bool
     */
    protected function pkcs7Sign(
        string $unsigned,
        string $out,
        array $headers = []
    ): bool {
        if ($this->certificateData && $this->privateKey) {
            return openssl_pkcs7_sign($unsigned, $out, $this->certificateData, [$this->privateKey, $this->password], $headers);
        }

        return false;
    }

    /**
     * @param string $certificate
     * @param string $password
     *
     * @return array|null
     */
    protected function retrieveCertificateData(string $certificate, string $password): ?array
    {
        if (file_exists($certificate)) {
            $pkcs12 = file_get_contents($certificate);
            $certs = [];
            if (openssl_pkcs12_read($pkcs12, $certs, $password)) {
                //$this->certificateData = openssl_x509_read($certs['cert']);
                //$this->privateKey = openssl_pkey_get_private($certs['pkey'], $password);
                $this->certificateData = $certs['cert'];
                $this->privateKey = $certs['pkey'];
                $this->password = $password;
            } else {
                throw new RuntimeException('Unable to open the certificate with the given passphrase');
            }
        } else {
            throw new RuntimeException('Unable to find the certificate');
        }

        return null;
    }
}
