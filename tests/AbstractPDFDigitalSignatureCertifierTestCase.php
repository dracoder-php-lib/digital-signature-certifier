<?php

namespace Dracoder\DigitalSignatureCertifier\Test;

use PHPUnit\Framework\TestCase;

class AbstractPDFDigitalSignatureCertifierTestCase extends TestCase
{
    //folders
    protected const EXAMPLES_FOLDER = __DIR__.DIRECTORY_SEPARATOR.'examples'.DIRECTORY_SEPARATOR;
    protected const CERTIFICATES_FOLDER = self::EXAMPLES_FOLDER.DIRECTORY_SEPARATOR.'certificate'.DIRECTORY_SEPARATOR;
    protected const INPUT_FOLDER = self::EXAMPLES_FOLDER.DIRECTORY_SEPARATOR.'input'.DIRECTORY_SEPARATOR;
    protected const OUTPUT_FOLDER = self::EXAMPLES_FOLDER.DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR;
    //files
    protected const TEXT_EXAMPLE_FILE = self::INPUT_FOLDER.'example.txt';
    protected const OUTPUT_TEXT_FILE = self::OUTPUT_FOLDER.'example_signed.txt';
    protected const PDF_EXAMPLE_FILE = self::INPUT_FOLDER.'pdf_original.pdf';
    protected const OUTPUT_PDF_FILE = self::OUTPUT_FOLDER.'pdf_signed.pdf';
    protected const CERTIFICATE_FILE = self::CERTIFICATES_FOLDER.'cert.pfx';
    protected const CERTIFICATE_PASSWORD = 'apples';
}
