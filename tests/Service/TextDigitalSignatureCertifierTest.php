<?php

namespace Dracoder\DigitalSignatureCertifier\Test\Service;

use Dracoder\DigitalSignatureCertifier\Service\TextDigitalSignatureCertifier;
use Dracoder\DigitalSignatureCertifier\Test\AbstractPDFDigitalSignatureCertifierTestCase;

class TextDigitalSignatureCertifierTest extends AbstractPDFDigitalSignatureCertifierTestCase
{
    public function testSign(): void
    {
        if (file_exists(self::OUTPUT_TEXT_FILE)) {
            unlink(self::OUTPUT_TEXT_FILE);
        }
        $certifier = new TextDigitalSignatureCertifier(self::CERTIFICATE_FILE, self::CERTIFICATE_PASSWORD);
        $result = $certifier->signFile(
            self::TEXT_EXAMPLE_FILE,
            self::OUTPUT_TEXT_FILE,
            [
                'From' => 'john.doe@example.com',
                'To' => 'C.G. <president@example.com>',
                'Subject' => 'Confidential',
            ]
        );
        self::assertTrue($result);
        self::assertFileExists(self::OUTPUT_TEXT_FILE);
    }
}
