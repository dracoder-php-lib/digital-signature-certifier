<?php

namespace Dracoder\DigitalSignatureCertifier\Test\Service;

use Dracoder\DigitalSignatureCertifier\Service\PdfDigitalSignatureCertifier;
use Dracoder\DigitalSignatureCertifier\Test\AbstractPDFDigitalSignatureCertifierTestCase;

class PDFDigitalSignatureCertifierTest extends AbstractPDFDigitalSignatureCertifierTestCase
{
    public function testSign(): void
    {
        $certifier = new PdfDigitalSignatureCertifier(self::CERTIFICATE_FILE, self::CERTIFICATE_PASSWORD);
        $certifier->signFile(self::PDF_EXAMPLE_FILE, self::OUTPUT_PDF_FILE);

        self::assertFileExists(self::OUTPUT_PDF_FILE);
    }
}
